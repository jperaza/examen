import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Fotos } from '../Model/Fotos';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  apiURL: string = 'https://misfotos.com/api/fotos';

  subirImagen(foto:Fotos){

    return this.http.post(this.apiURL,foto);
    

  }

}
