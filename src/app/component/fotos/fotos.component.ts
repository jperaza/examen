import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Fotos } from '../../Model/Fotos';

import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.component.html',
  styleUrls: ['./fotos.component.css']
})
export class FotosComponent {


   fotos = [

    { name: 'foto 1' , urlImage: 'https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/584eb6e95cafe885c4415f12/navidad0.jpg'  },
    { name: 'foto 2' , urlImage: 'https://www.lavanguardia.com/r/GODO/LV/p5/WebSite/2018/12/25/Recortada/img_ellopart_20181224-120211_imagenes_lv_getty_arbol_navidad-031-kr1B-U453762714522PLE-992x558@LaVanguardia-Web.jpg' },
    { name: 'foto 3' , urlImage: 'https://www.lavanguardia.com/r/GODO/LV/p5/WebSite/2018/12/25/Recortada/20181224-636812610260717751_20181224151203444-kr1B--656x494@LaVanguardia-Web.jpg'  },
    { name: 'foto 4' , urlImage: 'https://estaticos.muyinteresante.es/media/cache/760x570_thumb/uploads/images/gallery/584eb6e95cafe885c4415f12/navidad8.jpg' },
    { name: 'foto 5' , urlImage: 'https://img.freepik.com/vector-gratis/letras-feliz-navidad-brillantes-confeti-brillantes-adornos_1262-16808.jpg?size=338&ext=jpg'  },
    { name: 'foto 6' , urlImage: 'https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2018/12/navidad_ingl%C3%A9s.jpg' }
   ];

   

  constructor(private service:ServiceService) { }

  enviarPost(forma:NgForm){
    
    this.fotos.push({
      'name': forma.form.value.nombre,
      'urlImage': forma.form.value.urlImage
    });

    let fotosObject = new Fotos(
      forma.form.value.nombre,
      forma.form.value.urlImage
    );


    this.service.subirImagen(fotosObject).subscribe( (data:any) =>{

      

    }, 
    (error) => {
      
      alert('Error al subir el archivo')

    }
    );



  }

}
